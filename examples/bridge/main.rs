use qmetaobject::prelude::*;

use x_logger;
use x_gui;
use x_quick;
use x_ipy;

qrc!(register_resources, "/" {
    "examples/bridge/main.qml",
});

fn main() {

    register_resources();

    x_logger::init();
     x_quick::init();
       x_ipy::init();

let command = r#"

from qtpy import QtCore, QtQml

from jupyter_backend_qtquick.backend_qtquick_item import InterpreterItem

QtQml.qmlRegisterType(InterpreterItem, "xQuick.Ipy", 1, 0, "Jupyter")
"#;

    x_ipy::interpret(command);

    let mut engine = x_gui::xApplicationEngine::new();
    engine.set_application_name("x-ipy Test".into());
    engine.set_organisation_name("inria".into());
    engine.set_organisation_domain("fr".into());
    engine.add_import_path("qrc:///qml".into());
    engine.add_import_path("qrc:///src".into());
    engine.set_source("qrc:///examples/bridge/main.qml".into());
    engine.exec();
}
