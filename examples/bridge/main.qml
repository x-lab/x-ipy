import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15
import QtQuick.Window   2.15

import xLogger         1.0 as L

import xQuick          1.0 as X
import xQuick.Controls 1.0 as X
import xQuick.Fonts    1.0 as X
import xQuick.Style    1.0 as X
import xQuick.Ipy      1.0 as X

X.Application {

    id: window;

    width: 300;
    height: 400;

    Page {
        anchors.fill: parent;

        X.Jupyter {
            id: interpreter;
            focus: true;
            fillColor: "#000000"
            anchors.fill: parent;
        }
    }

    Component.onCompleted: {
        interpreter.initInterpreter();
        interpreter.forceActiveFocus();
    }
}
